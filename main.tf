terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.14.0"
    }
  }
}

provider "aws" {
  # use paris region
  region = "eu-west-3"
}

locals {
  key_path = "/media/high-sparrow/Documents/studies/Master/.keys/outside_use_aws.pem"
  user     = "ubuntu"
}

# create security group
## allow port 22 for ssh
resource "aws_security_group" "allow_ssh" {
  name        = "allow_sshv2"
  description = "Allow ssh inbound traffic and all outbound traffic"
  vpc_id      = data.aws_vpc.default.id
  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name   = "allow_sshv2"
    source = "terraform workshop"
  }
}
## allow port 80 for http
resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow http inbound traffic and all outbound traffic"
  vpc_id      = data.aws_vpc.default.id
  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name   = "allow_http"
    source = "terraform workshop"
  }
}
# allow 3000 && 8080 react port
resource "aws_security_group" "allow_react" {
  name        = "allow_react"
  description = "Allow react inbound traffic and all outbound traffic"
  vpc_id      = data.aws_vpc.default.id
  ingress {
    description = "react from VPC"
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "juju from VPC"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "minikube dashboard"
    from_port   = 46833
    to_port     = 46833
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name   = "allow_react_juju"
    source = "terraform workshop"
  }
}
# create the instance
# resource "aws_spot_instance_request" "dev_server" {
#   ami               = data.aws_ami.jisa_ami.id # default ami is the one you own with jisa code
#   instance_type     = var.instance_type        # default value is t3.xlarge
#   security_groups   = [aws_security_group.allow_ssh.name, aws_security_group.allow_http.name, aws_security_group.allow_react.name]
#   spot_price        = data.aws_ec2_spot_price.spot_price.spot_price
#   key_name          = "paris_default"
#   availability_zone = "eu-west-3a"
#   spot_type         = "one-time"
#   user_data         = filebase64("userdata.sh")
#   ebs_block_device {
#     device_name           = "/dev/sda1"
#     volume_size           = 30
#     volume_type           = "gp2"
#     delete_on_termination = true
#   }

#   provisioner "local-exec" {
#     command = "echo ${templatefile("inventory/template.yaml", { spot_instance_ip : self.public_ip, spot_instance_user : local.user, spot_instance_key : local.key_path })} > inventory/hosts.yaml"
#   }


#   tags = {
#     Name   = "dev server"
#     source = "terraform workshop"
#   }
# }

# instead of using spot request, we will create an instance
# defined as a spot instance from the beginning
resource "aws_instance" "dev_server" {
  ami               = data.aws_ami.jisa_ami.id # default ami is the one you own with jisa code
  instance_type     = var.instance_type        # default value is t3.xlarge
  security_groups   = [aws_security_group.allow_ssh.name, aws_security_group.allow_http.name, aws_security_group.allow_react.name]
  key_name          = "outside_use"
  availability_zone = "eu-west-3a"
  # user_data         = filebase64("userdata.sh") ansible will handle this
  ebs_block_device {
    device_name           = "/dev/sda1"
    volume_size           = 30
    volume_type           = "gp2"
    delete_on_termination = true
  }
  instance_market_options {
    market_type = "spot"
    spot_options {
      max_price = data.aws_ec2_spot_price.spot_price.spot_price
      # spot_instance_type = "one-time"
    }
  }

  provisioner "local-exec" {
    command = "echo \"${templatefile("templates/template.yaml", { spot_instance_ip : self.public_ip, spot_instance_user : local.user, spot_instance_key : local.key_path })}\" > inventory/hosts.yaml && python3 add_ssh_config.py ${self.public_ip} ${local.user} ${local.key_path}"
  }

  tags = {
    Name   = "dev server"
    source = "terraform workshop"
  }
}