deploy-machine: provision bring-docker bring-k8s 

provision:
	@echo "Provisioning..."
	@terraform init
	@terraform apply -auto-approve
	@echo "Provisioning done."

destroy:
	@echo "Destroying..."
	@terraform destroy -auto-approve
	@echo "Destroying done."

bring-k8s:
	@echo "Configuring Kubernetes..."
	@ansible-playbook -i inventory/hosts.yaml playbooks/minikube.yml
	@echo "Kubernetes configured."

bring-docker:
	@echo "Configuring Docker..."
	@ansible-playbook -i inventory/hosts.yaml playbooks/docker.yml
	@echo "Docker configured."

