output "public_ip" {
  description = "The public IP of the instance"
  value       = aws_instance.dev_server.public_ip
}

output "spot_price" {
  description = "The spot price of the instance"
  value       = data.aws_ec2_spot_price.spot_price
}