data "aws_vpc" "default" {
    default = true
}

data "aws_ami" "jisa_ami" {
    most_recent = true
    filter {
        name = "image-id"
        values = ["ami-05b5a865c3579bbc4"]
    }
}

data "aws_ec2_spot_price" "spot_price" {
  instance_type     = "t3.xlarge"
  availability_zone = "eu-west-3a"

  filter {
    name   = "product-description"
    values = ["Linux/UNIX"]
  }
}

